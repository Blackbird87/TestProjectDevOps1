<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

Welcome! Enter your name here:

<html:form action="/hello">
<html:text name="helloForm" property="name" />
<html:submit />

<html:errors/>
</html:form>
