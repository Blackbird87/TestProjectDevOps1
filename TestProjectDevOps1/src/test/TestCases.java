package test;

import org.junit.Test;

import bg.forms.HelloTestForm2;
import junit.framework.Assert;

public class TestCases{

	@Test
	public void checkIfNamesAreEqual()
	{
		HelloTestForm2 form2 = new HelloTestForm2();
		Assert.assertEquals("DragoIvanov", form2.getName()+form2.getLastName());
	
	}
	
}
