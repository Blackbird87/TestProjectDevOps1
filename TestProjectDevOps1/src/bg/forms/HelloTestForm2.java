package bg.forms;

import org.apache.struts.action.ActionForm;

public class HelloTestForm2 extends ActionForm{
	
	private static final long serialVersionUID = 1L;
	private String name = "Drago";
	private String lastName = "Ivanov";
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	

}
