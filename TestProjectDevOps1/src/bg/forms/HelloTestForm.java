package bg.forms;

import org.apache.struts.action.ActionForm;

public class HelloTestForm extends ActionForm{
	
	private static final long serialVersionUID = 1L;
	private String name;
	private String password;
	private String username2 ; 
	
	public String getUsername2() {
		return username2;
	}
	public void setUsername2(String username2) {
		this.username2 = username2;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
