package bg.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import bg.forms.HelloTestForm;

public class HelloTestAction extends Action{
	
	public ActionForward execute(ActionMapping mapping,ActionForm form,
			HttpServletRequest request,HttpServletResponse response)
	        throws Exception {
			
			HelloTestForm helloForm = (HelloTestForm)form;
			
			request.setAttribute("helloUser", "Hello " + helloForm.getName());
			
			
			return mapping.findForward("success");
		}
	

}
